---
title: "Table Test"
date: 2021-02-16T13:35:11-05:00
draft: false
---
{{< include-github-markdown >}}
# This section uses styling from 98.css
{{< begin-github-markdown >}}
# This `<div>` is formatted using Github-Flavored Markdown
| Col 1 | Col 2 | Col 3 | Col 4 |
|-------|-------|-------|-------|
| Column | Really really really really wide column | | That Last one was empty |
| Thing 01 | Thing 02 | Thing 03 | Thing 04 |
| 01 | 02 | 03 | 04 |

{{< end-github-markdown >}}

Aaaaand back to 98.css styling

# testing

***Remember to use reddit spacing!***
