# Gothic Possum Theme

The theme for [my site](https://gothicpossum.neocities.org) (not actually installed there yet :c)

Forked from [github.com/Vimux/blank](https://github.com/Vimux/blank/)

Uses [98.css](https://jdan.github.io/98.css/)

## Preview Image
![](images/20210215-WIP-preview.png)
*\*WIP*

## TODO

* Change the background image to being defined in `config.toml`
	* Need to lookup how that's done
* Make the window layouts more consistent
* Change *all* `.Date`'s to ISO 8601 format
* ~~Add support for my stupid images in the footer~~
* Figure out how to style tables for my blinkie gallery
	* Create a shortcode to iterate through a given directory
	* Every 3rd/4th element, start a new line?
* Add Xe's Gruvbox instead of GHFM?
* Add Chicago95 Icons to parts of the site (depending on license terms)
* **Future Stretch Goal Idea**: Convert to a Tumblr theme

## License

This theme is released under the [MIT license](https://github.com/Vimux/blank/blob/master/LICENSE).
